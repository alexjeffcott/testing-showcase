import React from 'react'
import { ExampleWrapper } from './components'
import './App.css'

const App = ({ isOdd }) => (
  <div className="App">
    <header className="App-header">
      <p>Hello React Testing Examples!!!</p>
      <ExampleWrapper isOdd={isOdd} />
    </header>
  </div>
)

const AppWrapper = () => <App isOdd={Math.floor(Math.random() * 10) % 2 !== 0} />

export { App }
export default AppWrapper

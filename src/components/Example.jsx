import React from 'react'

const Example = ({ name }) => <h1>{name}</h1>

const ExampleWrapper = ({ isOdd }) => <Example name={isOdd ? 'odd' : 'even'} />

export { ExampleWrapper }
export default ExampleWrapper

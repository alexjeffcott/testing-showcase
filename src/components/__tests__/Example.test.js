import React from 'react'
import { create } from 'react-test-renderer'
import { ExampleWrapper } from '..'
import ExampleWrapperMocks from '../__mocks__/ExampleWrapperMocks'

describe('ExampleWrapper component', () => {
  it.each(ExampleWrapperMocks)('matches Subtitle snapshot %#', props => {
    const component = create(<ExampleWrapper {...props} />)

    const tree = component.toJSON()
    expect(tree).toMatchSnapshot()
  })
})

import { format, distanceInWordsToNow } from 'date-fns'
import us from 'date-fns/locale/en'
import de from 'date-fns/locale/de'

const locales = Object.freeze({ us, de })

const formatDate = (passedDate, locale) => {
  const fnsDate = new Date(passedDate)
  return format(fnsDate.toUTCString(), `MMM, DD YYYY`, {
    locale: locales[locale],
  })
}

const getDistanceInWordsToNow = (epochMillisPublished, locale) =>
  distanceInWordsToNow(Number(epochMillisPublished), {
    addSuffix: true,
    locale: locales[locale],
  })

export { formatDate, getDistanceInWordsToNow, locales }

import { getDistanceInWordsToNow } from '../dates'

describe(`convertToNextDates`, () => {
  test(`should calculate distances between provided date in Milliseconds and today and return them in the users local`, () => {
    const oneDayInMilliseconds = 86400000
    const todayInMilliseconds = new Date().getTime()
    const twoDaysAgoInMilliseconds = todayInMilliseconds - oneDayInMilliseconds * 2
    const formatedDeDate = `VOR 2 TAGEN`
    const formatedEnDate = `2 DAYS AGO`
    expect(getDistanceInWordsToNow(twoDaysAgoInMilliseconds, `de`).toUpperCase()).toEqual(formatedDeDate)
    expect(getDistanceInWordsToNow(twoDaysAgoInMilliseconds, `us`).toUpperCase()).toEqual(formatedEnDate)
  })
})

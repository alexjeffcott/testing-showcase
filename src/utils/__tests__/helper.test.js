import { helper } from '..'
import helperMocks from '../__mocks__/helperMocks'

describe('helper function', () => {
  it.each(helperMocks)('helper %#', (ins, outs) => {
    expect(helper(...ins)).toEqual(outs)
  })
})

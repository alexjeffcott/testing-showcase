import { formatDate } from '..'
import formatDateMocks from '../__mocks__/formatDateMocks'

describe('formatDate function', () => {
  it.each(formatDateMocks)('formatDate %#', (ins, outs) => {
    expect(formatDate(...ins)).toEqual(outs)
  })
})

export default [
  [[`03 OCT 2018`, `de`], `Okt, 03 2018`],
  [[`03 OCT 2018`, `us`], `Oct, 03 2018`],
  [[`03 OCT 2018`, `naughty string`], `Oct, 03 2018`],
  [[`03 OCT 2018`, 123], `Oct, 03 2018`],
  [[`03 OCT 2018`, undefined], `Oct, 03 2018`],
  [[`03 OCT 2018`, null], `Oct, 03 2018`],
  [[`03 may 18`, null], `May, 03 2018`],
  [[`03 may 18`, 'de'], `Mai, 03 2018`]
]

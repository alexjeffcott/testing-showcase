const oneDay = 86400000
const today = new Date().getTime()
const lunaLanding = -14186540000

export default [
  [[today - oneDay * 2, `de`], `vor 2 Tagen`],
  [[today - oneDay * 2, `us`], `2 days ago`],
  [[lunaLanding, `de`], `vor fast 50 Jahren`],
  [[lunaLanding, `us`], `almost 50 years ago`]
]

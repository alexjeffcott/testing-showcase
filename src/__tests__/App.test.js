import React from 'react'
import { create } from 'react-test-renderer'
import { App } from '../App'
import AppMocks from '../__mocks__/AppMocks'

describe('App component', () => {
  it.each(AppMocks)('matches Subtitle snapshot %#', props => {
    const component = create(<App {...props} />)
    const tree = component.toJSON()
    expect(tree).toMatchSnapshot()
  })
})

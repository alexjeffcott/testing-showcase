# Testing Showcase

## What is this showcase for?

To provide a place for working examples of testing to:

- facilitate discussion on what working practices should be
- centralise and document these working practices
- make it easier to experiment freely and discuss these experiments
- speed up training for people interested in learning about training
- make onboarding of new developers faster and more efficient

## getting started with this testing showcase

- clone the repo
- yarn install
- yarn start

## Here are some useful VSCode snippets

```js
{
  "Snapshot": {
    "prefix": "snapshot",
    "body": [
      "import React from 'react'",
      "import {create} from 'react-test-renderer'",
      "import {$1} from '..'",
      "import $1Mocks from '../__mocks__/$1Mocks'",
      "",
      "describe('${1:Component} component', () => {",
      "\tit.each($1Mocks)('matches Subtitle snapshot %#', props => {",
      "\t\tconst component = create(",
      "\t\t\t<${1:Component} {...props}/>",
      "\t\t)",
      "\t\tconst tree = component.toJSON()",
      "\t\texpect(tree).toMatchSnapshot()",
      "\t})",
      "})"
    ],
    "description": "Creates a snapshot test"
  },
  "Test": {
    "prefix": "test",
    "body": [
      "import {$1} from '..'",
      "import $1Mocks from '../__mocks__/$1Mocks'",
      "",
      "describe('$1 function', () => {",
      "\tit.each($1Mocks)('$1 %#', (ins, outs) => {",
      "\t\texpect($1(...ins)).toEqual(outs)",
      "\t})",
      "})"
    ],
    "description": "Creates a test all template"
  },
  "Mock Data for unit test": {
    "prefix": "testmock",
    "body": ["export default [", "\t[['inVal', 'inVal'], 'outVal']", "]"],
    "description": "Creates a mock data template"
  },
    "Mock Data for snapshot test": {
    "prefix": "snapshotmock",
    "body": ["export default [", "\t[{prop: value}]", "]"],
    "description": "Creates a mock data template"
  }
}
```
